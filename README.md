# LPEncyclopediaBuildLib #

A [Lakitu](https://bitbucket.org/maccosslab/lakitu) pipeline that runs msconvert, encyclopedia, and skyline to query DIA Data with a spectral library.

## Building
```bash
./build.sh release
```

## Testing

```bash
lpencyclopediabuildlib/interface.py --workers 100 MainTask --run-id smoke_test --ms-set s3://atkeller.lakitu.public/pipeline_tests/test_lpencyclopediabuildlib/smoke/narrow_msfiles.txt --msconvert-config s3://atkeller.lakitu.public/pipeline_tests/test_lpencyclopediabuildlib/smoke/msconvert_config.txt --fasta-file s3://atkeller.lakitu.public/pipeline_tests/test_lpencyclopediabuildlib/smoke/human_20150911_uniprot_sp.fasta --query-elib-file s3://atkeller.lakitu.public/pipeline_tests/test_lpencyclopediabuildlib/smoke/phl004_canonical_sall_osw.dlib --precursor-tolerance 10.0 --precursor-tol-unit ppm --fragment-tolerance 10.0 --fragment-tol-unit ppm --rt-align false --blib false --out-lib-name merged
```
