from lpmsconvert.abstract import BuildToolDirectoryBase
from luigi.util import inherits

from abstract import RunId
from buildworkingdirectory import BuildWorkingDirectory


@inherits(RunId)
class BuildToolDirectory(BuildToolDirectoryBase):
    def requires(self):
        return {'base_directory': self.clone(BuildWorkingDirectory)}
