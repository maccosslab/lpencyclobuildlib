import version
__version__ = "{}.{}.{}".format(version.MAJOR_VERSION, version.MINOR_VERSION, version.PATCH_VERSION)
