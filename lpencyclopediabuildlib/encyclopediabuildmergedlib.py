import luigi
from lpmsconvert.abstract import S3PathParameter
from luigi.util import inherits

from abstract import EncyclopediaBuildMergedLibBase
from abstract import RunId
from encyclopediasingle import EncyclopediaSingle


@inherits(RunId)
class EncyclopediaBuildMergedLib(EncyclopediaBuildMergedLibBase):
    msconvert_config = S3PathParameter(description="S3 path to msconvert config")

    def requires(self):
        # requires encyclopedia single run on all input mzml files
        return {ms_file: self.clone(EncyclopediaSingle, ms_file=ms_file, msconvert_config=self.msconvert_config)
                for ms_file in self.ms_path_list}


if __name__ == '__main__':
    luigi.run()
