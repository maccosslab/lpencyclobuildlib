import posixpath
from datetime import datetime

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakitu.aws.batch import BatchTask
from lakitu.aws.s3helpers import bs3, bucket_and_key_from_s3
from lakituapi.pipeline import LakituAwsConfig
from lpmsconvert.abstract import S3PathParameter
from luigi.util import inherits


def read_from_s3(s3_path):
    """
    Read a file from an S3 source.

    A string of bytes is returned. To read and iterate over a text file, for example, you could do this:

    >>> import StringIO
    >>> msg = read_from_s3('s3://bucket-name/key/foo.txt')
    >>> buf = StringIO.StringIO(msg)
    >>> for line in buf.readline():
    >>>     pass # Do stuff here

    :param s3_path: Path starting with s3://, e.g. 's3://bucket-name/key/foo.txt'
    :return: content : bytes
    """
    bucket, key = bucket_and_key_from_s3(s3_path)
    s3_object = bs3.get_object(Bucket=bucket, Key=key)
    body = s3_object['Body']
    return body.read()



def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


class RunId(luigi.Config):
    run_id = luigi.Parameter(default=datetime.now().strftime('%Y-%m-%d_%H-%M-%S'),
                             description="A unique identifier for storing data, including intermediate files and final"
                                         " output.")


class EncyclopediaParams(luigi.Config):
    query_elib_file = S3PathParameter(description='EncyclopeDIA: S3 path to elib library to query dia data with')
    fasta_file = S3PathParameter(description='EncyclopeDIA: S3 path to FASTA file')
    precursor_tolerance = luigi.FloatParameter(default=10.0,
                                               description='EncyclopeDIA: precursor ppm error (e.g. 10.0)')
    precursor_tol_unit = luigi.Parameter(default='ppm',
                                         description='EncyclopeDIA: {ppm,amu} default=ppm: Precursor tolerance unit.')
    fragment_tolerance = luigi.FloatParameter(default=10.0,
                                              description='EncyclopeDIA: fragment ppm error (e.g. 10.0)')
    fragment_tol_unit = luigi.Parameter(default='ppm',
                                         description='EncyclopeDIA: {ppm,amu} default=ppm: Fragment tolerance unit.')


@inherits(EncyclopediaParams)
class EncyclopediaSingleBase(BatchTask):

    @property
    def estimated_jvm_memory(self):
        """
        Heap memory requirements in Mb
        """
        container_reserved = 4 * 1024
        heap_memory = self.estimated_container_memory_mb - container_reserved
        if heap_memory < 64:
            raise Exception("The container estimate for memory was too low")
        return heap_memory

    @property
    def estimated_container_memory_mb(self):
        """
        Estimates the memory requirements of this job based on the input files.
        """
        minimum_memory = 4 * 1024
        maximum_memory = 1952 * 1024  # The largest EC2 instance available has 1952 GiB memory
        # TODO add estimation based on file contents
        estimated_memory = 32 * 1024
        if estimated_memory > maximum_memory:
            raise Exception(
                "Memory estimate to run Percolator exceeds maximum available ({} GiB)".format(maximum_memory / 1024))
        return max(estimated_memory, minimum_memory)

    @property
    def tool_wd(self):
        return self.input()['tool_wd'].path

    @property
    def elib_in(self):
        return self.input()['library_s3'].path

    @property
    def fasta_in(self):
        return self.input()['fasta_s3'].path

    @property
    def mzml_in(self):
        return posixpath.join(self.tool_wd, posixpath.basename(self.input()['mzml_s3'].path))

    # BatchTask Interface
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn

    @property
    def job_def(self):
        return {
            'jobDefinitionName': 'encyclopedia_0_6_14',
            'type': 'container',
            'containerProperties':
                {
                    'image': 'atkeller/s3wrap_encyclopedia:v0.6.14_cv1',
                    'memory': self.estimated_container_memory_mb,
                    'vcpus': 8  # allocate at least one core for this
                }
        }
    # /BatchTask Interface

    # AWSTaskBase Interface
    command_base = ['s3wrap.py',
                    'encyclopedia',
                    '-i', 'Ref::mzml_in',
                    '-l', 'Ref::elib_in',
                    '-f', 'Ref::fasta_in',
                    '-ptol', 'Ref::prec_tol',
                    '-ptolunits', 'Ref::prec_tol_units',
                    '-ftol', 'Ref::frag_tol',
                    '-ftolunits', 'Ref::frag_tol_units',
                    '-acquisition', 'DIA']

    @property
    def parameters(self):
        return {
            'mzml_in': self.mzml_in,
            'elib_in': self.elib_in,
            'fasta_in': self.fasta_in,
            'prec_tol': str(self.precursor_tolerance),
            'prec_tol_units': str(self.precursor_tol_unit),
            'frag_tol': str(self.fragment_tolerance),
            'frag_tol_units': str(self.fragment_tol_unit)
        }

    @property
    def environment(self):
        additional_uploads = ""
        # get the mzml prefix: e.g. s3://bucket_name/mzml_dir/mzml_file.mzml -> bucket_name/mzml_dir/mzml_file
        mzml_prefix = remove_prefix(posixpath.splitext(self.mzml_in)[0], prefix='s3://')
        additional_uploads += mzml_prefix + '.dia'
        additional_uploads += ':' + mzml_prefix + '.mzML.elib'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.delta_rt.pdf'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.log'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.rt_fit.pdf'
        additional_uploads += ':' + mzml_prefix + '.mzML.encyclopedia.txt.rt_fit.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.features.txt'
        additional_uploads += ':' + mzml_prefix + '.mzML.first_round.txt'

        return [{'name': 's3wrap_extra_uploads', 'value': additional_uploads},
                {'name': 'JAVA_OPTIONS', 'value': '-Xmx{memory}m'.format(memory=self.estimated_jvm_memory)}]

    @property
    def downloadable_outputs(self):
        o = self.output()
        return [t for k, t in o.iteritems() if k not in ('output_directory',)]
    # /AWSTaskBase Interface

    # Luigi Interface
    ms_file = S3PathParameter()

    def requires(self):
        """
        Return a dictionary with the following members:

        {
            'tool_wd': BuildToolDirectory(toolname='encyclopedia'),
            'library_s3': UploadInputFileToS3(source_file=self.query_elib_file),
            'fasta_s3': UploadInputFileToS3(source_file=self.fasta_file),
            'mzml_s3': MSConvert(msc_in_path=self.ms_file)
        }
        """
        raise NotImplementedError("Abstract method not defined")

    def run(self):
        # copy the input mzml to the tool directory so that s3_wrap copies output files back into the
        # tool directory. Otherwise it will copy into the source directory of the mzml file, because encyclopedia
        # outputs all files into the same directory as the input mzml.
        print "Copying {} -> {}".format(self.input()['mzml_s3'].path, self.mzml_in)
        s3helpers.cp_s3(self.input()['mzml_s3'].path, self.mzml_in)

        self.run_batch()

    def output(self):
        out_dir = self.tool_wd
        mzml_basename = posixpath.basename(self.mzml_in)
        mzml_prefix = posixpath.splitext(mzml_basename)[0]
        return {'output_directory': s3.S3Target(out_dir),
                'elib': s3.S3Target(posixpath.join(out_dir, mzml_basename + '.elib')),
                'dia': s3.S3Target(posixpath.join(out_dir, mzml_prefix + '.dia')),
                'encyc_txt': s3.S3Target(posixpath.join(out_dir, mzml_basename + '.encyclopedia.txt')),
                'features_txt': s3.S3Target(posixpath.join(out_dir, mzml_basename + '.features.txt')),
                }
    # /Luigi Interface


class EncyclopediaLibexportParams(luigi.Config):
    rt_align = luigi.ChoiceParameter(choices=['true', 'false'], var_type=str, default='true',
                                     description='EncyclopeDIA: {true,false}: Perform retention time alignment?')
    blib = luigi.ChoiceParameter(choices=['true', 'false'], var_type=str, default='false',
                                 description="EncyclopeDIA: {true,false}: If true, output blib file, if false (default) output elib.")
    out_lib_name = luigi.Parameter(default='merged',
                                   description="EncyclopeDIA: Filename (not including extension) to give output library file.")


@inherits(EncyclopediaParams)
@inherits(EncyclopediaLibexportParams)
class EncyclopediaBuildMergedLibBase(BatchTask):

    @property
    def estimated_jvm_memory(self):
        """
        Heap memory requirements in Mb
        """
        container_reserved = 4 * 1024
        heap_memory = self.estimated_container_memory_mb - container_reserved
        if heap_memory < 64:
            raise Exception("The container estimate for memory was too low")
        return heap_memory

    @property
    def estimated_container_memory_mb(self):
        """
        Estimates the memory requirements of this job based on the input files.
        """
        minimum_memory = 4 * 1024
        maximum_memory = 1952 * 1024  # The largest EC2 instance available has 1952 GiB memory
        # TODO add estimation
        estimated_memory = 16 * 1024
        if estimated_memory > maximum_memory:
            raise Exception(
                "Memory estimate to run Percolator exceeds maximum available ({} GiB)".format(maximum_memory / 1024))
        return max(estimated_memory, minimum_memory)

    @property
    def lib_suffix(self):
        return '.blib' if self.blib == 'true' else '.elib'

    @property
    def encyclopedia_s3_dir(self):
        # directory with encyclopedia outputs
        e_path = self.input().values()[0]['output_directory'].path
        if not e_path.endswith('/'):
            e_path += '/'
        return e_path

    @property
    def query_elib_s3(self):
        # location on s3 of the query elib file
        return self.requires().values()[0].input()['library_s3'].path

    @property
    def query_fasta_s3(self):
        # location on s3 of the query fasta file
        return self.requires().values()[0].input()['fasta_s3'].path

    # BatchTask Interface
    job_queue_arn = LakituAwsConfig.linux_job_queue_arn

    @property
    def job_def(self):
        return {
            'jobDefinitionName': 'encyclopedia_0_6_14_mergelib',
            'type': 'container',
            'containerProperties':
                {
                    'image': 'atkeller/s3wrap_encyclopedia:v0.6.14_cv1',
                    'memory': self.estimated_container_memory_mb,
                    'vcpus': 2  # allocate two cores for merging
                }
        }
    # /BatchTask Interface

    # AWSTaskBase Interface
    command_base = ['s3wrap.py',
                    'encyclopedia',
                    '-libexport',
                    '-i', 'Ref::in_directory',
                    '-o', 'Ref::out_library',
                    '-l', 'Ref::query_elib',
                    '-f', 'Ref::query_fasta',
                    '-ftol', 'Ref::frag_tol',
                    '-ftolunits', 'Ref::frag_tol_units',
                    '-a', 'Ref::rt_align',
                    '-blib', 'Ref::blib']

    @property
    def parameters(self):
        return {
            'in_directory': self.encyclopedia_s3_dir,
            'out_library': self.output().path,
            'query_elib': self.query_elib_s3,
            'query_fasta': self.query_fasta_s3,
            'frag_tol': str(self.fragment_tolerance),
            'frag_tol_units': str(self.fragment_tol_unit),
            'rt_align': self.rt_align,
            'blib': self.blib
        }

    @property
    def environment(self):
        return [
            {'name': 'JAVA_OPTS',
             'value': '-Xmx{memory}m'.format(memory=self.estimated_jvm_memory)}
        ]
    # /AWSTaskBase Interface

    # Luigi Interface
    # path to directory containing mzml files to analyze
    ms_path_list = luigi.ListParameter(description="A list of ms files to analyze on local system or S3")

    def requires(self):
        """
        Return a dictionary with the following members:
        {
            ms_file: self.clone(EncyclopediaSingle, ms_file=ms_file) for ms_file in self.ms_path_list
        }
        """
        raise NotImplementedError("Abstract method not defined")

    def output(self):
        return s3.S3Target(posixpath.join(self.encyclopedia_s3_dir, self.out_lib_name) + self.lib_suffix)
    # /Luigi Interface
