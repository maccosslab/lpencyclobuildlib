import os

import luigi
import luigi.contrib.s3 as s3
from lakitu.aws import s3helpers
from lakituapi.pipeline import LakituAwsConfig
from luigi.util import inherits

from abstract import RunId


@inherits(RunId)
class BuildWorkingDirectory(luigi.Task):
    """
    Build a working directory for the pipeline
    """
    s3_bucket = luigi.Parameter(default=LakituAwsConfig.s3_run_bucket,
                                description="The root s3 processing directory")

    def run(self):
        s3helpers.mkdir_s3(self.output().path)

    def output(self):
        return s3.S3Target(os.path.join("s3://", self.s3_bucket,
                                        "lpencyclopediabuildlib",
                                        "{}".format(self.run_id)) + '/')


if __name__ == '__main__':
    luigi.run()
