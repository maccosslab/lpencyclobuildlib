import StringIO
import posixpath

import luigi
from lakitu.aws.s3helpers import search_s3_prefix
from lpmsconvert.abstract import S3PathParameter
from luigi.util import inherits

from abstract import EncyclopediaParams, EncyclopediaLibexportParams
from abstract import RunId
from abstract import read_from_s3
from copyalltooutputs3dir import CopyAllToOutputS3Dir
from encyclopediabuildmergedlib import EncyclopediaBuildMergedLib


@inherits(RunId)
@inherits(EncyclopediaParams)
@inherits(EncyclopediaLibexportParams)
class MainTask(luigi.WrapperTask):
    ms_set = S3PathParameter(description="Either"
                                         " (1) an S3 URL pointing to an S3 folder containing MS files,"
                                         " (2) a comma-separated list of S3 URLs pointing to MS files,"
                                         " or (3) an S3 text file with an S3 path to an MS file"
                                         " on each line")
    msconvert_config = S3PathParameter(description="S3 path to msconvert config")

    def requires(self):
        encyc_task = self.clone(EncyclopediaBuildMergedLib, ms_path_list=self._get_ms_paths(),
                                                           msconvert_config=self.msconvert_config)
        yield encyc_task
        yield CopyAllToOutputS3Dir(run_id=self.run_id, seed_task=encyc_task)

    def _get_ms_paths(self):
        formats = ("mzml", "mzxml", "mz5", "mgf", "text", "ms2", "cms2", "raw", "d", "wiff")

        if self.ms_set.endswith('.txt'):
            # Read .txt from s3
            buf = StringIO.StringIO(read_from_s3(self.ms_set))
            s3_paths = [list_item.strip() for list_item in buf.readlines()]
        elif self.ms_set.endswith('/'):
            # Get listing of s3 files in this directory
            s3_paths = search_s3_prefix(self.ms_set)
        else:
            # Handle comma-separated list of S3 MS file paths
            s3_paths = [list_item.strip() for list_item in self.ms_set.split(',')]

        return [s3_path for s3_path in s3_paths if posixpath.splitext(s3_path)[-1][1:].lower() in formats]


if __name__ == '__main__':
    luigi.run()
