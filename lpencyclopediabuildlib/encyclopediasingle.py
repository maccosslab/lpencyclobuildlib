import luigi
from lpmsconvert.abstract import S3PathParameter
from luigi.util import inherits

from abstract import EncyclopediaSingleBase
from abstract import RunId
from buildtooldirectory import BuildToolDirectory
from msconvert import MSConvert
from uploadinputfiletos3 import UploadInputFileToS3


@inherits(RunId)
class EncyclopediaSingle(EncyclopediaSingleBase):
    msconvert_config = S3PathParameter(description="S3 path to msconvert config")

    def requires(self):
        return {'tool_wd': self.clone(BuildToolDirectory, toolname='encyclopedia'),
                'library_s3': self.clone(UploadInputFileToS3, source_file=self.query_elib_file),
                'fasta_s3': self.clone(UploadInputFileToS3, source_file=self.fasta_file),
                'mzml_s3': self.clone(MSConvert, msc_in_path=self.ms_file, msc_config=self.msconvert_config)}


if __name__ == '__main__':
    luigi.run()
