zlib=true
mz64=true
inten64=true
simAsSpectra=true
filter="peakPicking vendor msLevel=1-2"
filter="scanTime [3600.0,4200.0]"
filter="demultiplex optimization=overlap_only"
