import os
from subprocess import check_output

from setuptools import setup, find_packages

from lpencyclopediabuildlib.version import *


def get_version_string():
    try:
        output = check_output(["git", "describe", "--tags"])
    except OSError as e:
        if e.errno == os.errno.ENOENT:
            # clarify file not found error.
            raise Exception("git must be in path to install development version")
        else:
            # Something else went wrong while trying to run `check_output`
            raise
    parts = [part.strip() for part in output.split('-')]
    expected_tag = "{}.{}.{}".format(MAJOR_VERSION, MINOR_VERSION, PATCH_VERSION)
    if len(parts) == 1:
        git_tag = "{}".format(parts[0])
        git_version_string = git_tag
    else:
        git_tag, count, sha = parts[:3]
        git_version_string = "{}.dev{}+{}".format(git_tag, count, sha)

    if git_tag != expected_tag:
        print("WARNING: Git tag {} should match version in version.py {}".format(git_tag, expected_tag))
    return git_version_string


setup(
    name="lpencyclopediabuildlib",
    version=get_version_string(),
    description="A Lakitu pipeline for building a merged EncyclopeDIA library",
    author="Austin Keller",
    author_email="atkeller@uw.edu",
    license="Apache 2.0",
    url="https://bitbucket.org/maccosslab/lpencyclobuildlib",
    classifiers=['Development Status :: 3 - Alpha',
                 'Programming Language :: Python :: 2.7'],
    packages=find_packages(),
    install_requires=[
        'lakituapi==0.1.4',
        'lakitu==0.12.0',
        'lpmsconvert==0.2.0',
    ]
)
